import Vue from 'vue'
import {
  SET_NODE_TREE,
  CLEAR_NODE_TREE,
  ADD_NESTED_NODE,
  REMOVE_NESTED_NODE
} from "./mutations.js"

const state = {
  nodeTree: null
}

const getters = {
  nodeTree(state) {
    return state.nodeTree
  }
}

const mutations = {
  [SET_NODE_TREE](state, node) {
    state.nodeTree = node
  },

  [CLEAR_NODE_TREE](state) {
    state.nodeTree = null
  },

  [ADD_NESTED_NODE](state, { objectToUpdate, objectToMove}) {
    if(objectToUpdate.children) {
      objectToUpdate.children.push(objectToMove)
    } else {
      Vue.set(objectToUpdate, 'children', [objectToMove])
    }
  },

  [REMOVE_NESTED_NODE](state, { objectForRemove, parentObject }) {
    if(!parentObject.children) return
    const index = parentObject.children.findIndex(obj => obj === objectForRemove)

    if(index >= 0) parentObject.children.splice(index, 1)
    if(parentObject.children.length === 0) delete parentObject.children
  }
}

export default {
  state,
  getters,
  mutations
}
