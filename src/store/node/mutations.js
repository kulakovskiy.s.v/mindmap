export const SET_NODE_TREE = "set-node-tree"
export const CLEAR_NODE_TREE = "clear-node-tree"

export const ADD_NESTED_NODE = "add-nested-node"
export const REMOVE_NESTED_NODE = "remove-nested-node"
