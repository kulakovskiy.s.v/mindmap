export default function(nodeTreeObj) {
  const dataStr = Buffer.from(JSON.stringify(nodeTreeObj, null, 2)).toString("base64")

  const link = document.createElement('a')
  link.setAttribute('href', `data:application/octet-stream;base64,${dataStr}`)
  link.setAttribute('download', 'data.json')

  link.click()
}
