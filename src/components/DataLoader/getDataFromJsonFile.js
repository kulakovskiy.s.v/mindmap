export default function(file) {
  if (file.length && file[0].type === 'application/json') {
    return new Promise((res, rej) => {
      const reader = new FileReader()

      reader.onload = (e) => {
        try {
          const jsonData = JSON.parse(e.target.result)
    
          if(jsonData.data) {
            res(jsonData)
          } else {
            rej('Error: Invalid data format.')
          }
        } catch (error) {
          rej(error)
        }
      }
      
      reader.readAsText(file[0])
    })
  }

  throw new Error('Invalid file type.')
}
