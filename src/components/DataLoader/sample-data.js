export default {
  data: [
    {
      title:"Root element",
      children:[
        {
          title:"Nester element 1"
        },
        {
          title:"Nester element 1",
          children:[
            {
              title:"Nester element 2"
            },
            {
              title:"Nester element 2"
            }
          ]
        }
      ]
    }
  ]
}