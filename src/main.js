import Vue from 'vue'
import App from './App.vue'
import 'normalize.css';

import store from "./store"

Vue.filter('formatToJson', function (value) {
  return JSON.stringify(value, null, 2)
})

new Vue({
  store,
  render: h => h(App),
}).$mount('#app')
